package cl.ubb.kata.factorial.clase;



public class Primo {
	public boolean resultado;

	public boolean evaluarSiEsPrimo(int x){
		if(x==1){
			resultado= false;
		}
		if(x==2 || x==3){
			resultado= true;
		}
		return resultado;
	}

}
