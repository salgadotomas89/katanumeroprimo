package cl.ubb.kata.primo.test;

import static org.junit.Assert.*;

import org.junit.Test;

import cl.ubb.kata.factorial.clase.Primo;

public class PrimoTest {
	public boolean resultado;

	@Test
	public void alEvaluarNumero1DevuelveFalse() {
		//arrange
		Primo p = new Primo();
		//act
		resultado=p.evaluarSiEsPrimo(1);
		//assert
		assertEquals(false,resultado);
		
	}
	@Test
	public void alEvaluarNumero2DevuelveTrue() {
		//arrange
		Primo p = new Primo();
		//act
		resultado=p.evaluarSiEsPrimo(2);
		//assert
		assertEquals(true,resultado);
		
	}
	@Test
	public void alEvaluarNumero3DevuelveTrue() {
		//arrange
		Primo p = new Primo();
		//act
		resultado=p.evaluarSiEsPrimo(3);
		//assert
		assertEquals(true,resultado);
		
	}

}
